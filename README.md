# trackdechets-workflow

```mermaid
graph TD
  subgraph Préparation du BSD
    a1(Création d'un BSD) --> b0
    subgraph Informations émetteur
      b0{Case 1} --> b1
      b1(Ajout d'un numéro bis ? ) -->|Non| b2(Ajout d'un éco-organisme ?)
      style b1 fill:#f1c40f
      style b2 fill:#f1c40f
      b1 -.->|Oui| b11(J'ajoute un numéro bis)
      b11 -.-> b2
      b2 -->|Non| b3(Je choisis le type de producteur)
      b2 -.->|Oui| b21(J'ajoute un EO en choissant dans la liste)
      b21 -.->|autocomplétion| b22(type producteur = autre détenteur)
      b22 -.-> b3
      b3 --> b4("Je complète les infos entreprise (siret, nom, adresse, email)")
      b4 --> b5(Ajout d'une adresse chantier de collecte?)
      style b5 fill:#f1c40f
      b5 -.->|Oui| b51(Je complète les infos de l'adresse chantier)
      b5 -->|Non| b99
      b51 -.-> b99{Fin case 1}
      end
      b99 --> c0
    subgraph Case 2,3,4,5,6
      c0{Début case 2,3,4,5,6} --> c1
      c1(Entreposage provisoire ou reconditionnement prévu ?)
      c1-->|N| c12(Je complète les infos de l'entreprise de destination)
      style c1 fill:#f1c40f
      c12-->c2(Je complète les infos CAP le cas échant)
      c1 -.->|OUI| c11(Je complète les infos de l'entreprise d'entreposage)
      c11 -.-> c2
      c2 --> c3(Je complète le code D/R prévu)
      c3 --> c99{Fin case 2}
    end
    c99 --> d0
    subgraph Case 3,4,5,6
      d0{Début case 3,4,5,6} --> d1(code, dénomination du déchet, consistance)
      d1 --> d2(je complète les mentions ADR si besoin)
      d2-->d3(Je complète le conditionnement et le nbre de colis)
      d3 -->d4(Je complète la quantité)
      d4-->d99{fin case 3, 4, 5, 6}
    end
    d99-->e0
    subgraph Case 7
      e0{Début case 7}-->e1(Il y a-t-il un négociant ?)
      e1-.->|Oui|e11(j'ajoute les informations du négociant)
      e11-.->e12(j'ajout les infos du récépissé)
      e12-.->e99
      e1-->|non|e99{fin case 7}
    end
    e99-->f0
    
  end
```
