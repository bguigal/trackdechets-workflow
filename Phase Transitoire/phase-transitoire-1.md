```mermaid 
graph TD
    C1[BSD NUMÉRIQUE] --> |"Édition case 1 à 8"| C2
    C2[DRAFT] --> |Validation case 1 à 8| C3(SEALED)
    C3 -.->|signature case 8 et case 9 | C4[SENT]
    C4 -.->|signature case 10| C5[RECEIVED]
    C5 -->|signature case 11| C6[PROCESSED]
    C3(SEALED) --> CC1
    CC1[BSD PAPIER] -->|signature phase transitoire| C5
    classDef light opacity:30%;
    class C4 light