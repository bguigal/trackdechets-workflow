```mermaid
graph TD
    A1[BSD NUMÉRIQUE] --> |"Édition case 1 à 8"| A2
    A2[DRAFT] --> |Validation case 1 à 8| A3(SEALED)
    A3 -->|signature case 8 et case 9 | A4[SENT]
    A4 -->|signature case 10| A5[RECEIVED]
    A5 -->|signature case 11| A6[PROCESSED]