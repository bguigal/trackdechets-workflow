```mermaid

graph TD
    B1[BSD NUMÉRIQUE] -.-> |"Édition case 1 à 8"| B2
    B2[DRAFT] -.-> |Validation case 1 à 8| B3(SEALED)
    B3 -.->|signature case 8 et case 9 | B4[SENT]
    B4 -.->|signature case 10| B5[RECEIVED]
    B5 -->|signature case 11| B6[PROCESSED]
    BB1[BSD PAPIER] -->|signature phase transitoire| B5
    class B1,B2,B3,B4 light
    classDef light opacity:30%;